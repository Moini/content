<!-- This is for calls for donations, sponsorship, volunteers, etc. -->

## Elevator Pitch
* The hook
    - Something intriguing
* Grab their attention
    - A shocking statistic about the impact we've made
* Just dive right in
* Infuse it with humanity.
    - Who is the protagonist?
* Boil this section down to just 1-2 sentences max

## The Body
* Why Inkscape exists; why is it important
    - what problem do we aim to solve?
    - How effective are our current programs?
* What makes Inkscape unique?
    - What differentiates our nonprofit from others in the same space?
    - How are we going to succeed
* What our organization does
    - keep it short
    - Short success story
    - Do we have impact stories that are amazing?
* What needs to be done
    - Problem statement
    - How can someone get involved right now?
    - Open-ended questions to engage conversations

## The Wrap-Up
* Bring everything home with a very specific ask / call to action
    - Click here to volunteer
    - Volunteer today
    - Learn how you can help
    - Donate now
* What outcome do we want?
    - Matching donations for the next campaign?
    - Signup for recurring donations?
    - Share campaign on their social channels?
    - (May want to create multiple pitches depending on audience)
* Key take away, something memorable
* This section should also be short, no more than 2-3 sentences max
