# Inkscape Vectors Onboarding Survey

## Content
### Landing Page

Hello! Welcome to the Inkscape project! Thank you for your interest in the Vectors team - we are always happy for new teammates to join us. The mission of the Inkscape Vectors team is to broaden adoption, increase contributions, nurture partnerships, and deepen loyalty. Artists, designers, developers, marketers, students, and others are welcome to join the effort. You can find out about our work on the Inkscape [website](https://inkscape.org/*inkscape-vectors/) and on the GitLab [repository](https://gitlab.com/groups/inkscape/vectors/-/issues). And we would also love to know about you. This survey is for us to understand how you wish to contribute to Inkscape as part of the Vectors team, so that we can better help you get started :)

## Questions

1. I am an … 
    * Artist 
    * Graphics Designer 
    * Writer
    * Programmer 
    * Teacher 
    * Maker
    * Scientist 
    * Marketer
    * Project manager
    * Free/Libre and Open Source Software (F/LOSS) fan 
1. I think Inkscape teams should focus more on:  
1. I would like to contribute to Inkscape as part of the Vectors team by … 
    * Writing articles 
    * Writing tutorials
    * Managing social media communications 
    * Creating posters, illustrations and other marketing collateral 
    * Revising Inkscape app UX and UI 
    * Creating and processing videos 
    * Organizing events
    * Hosting podcats (you would love [Inkscope](https://www.youtube.com/watch?v=1J1r8a0emYc)!)
    * Programming (CSS)
    * Programming (Django) 
1. I found out about the Vectors team through …
1. You'll recognize me under this alias (on Inkscape chat or GitLab): 


### End Page

Thank you! We appreciate your time and interest in contributing to Inkscape and the Vectors team in particular. We'll get in touch with you as soon as we can. In the meanwhile, you're welcome to join our channel, [#team_vectors](https://chat.inkscape.org/channel/team_vectors) on our rocket chat instance.
