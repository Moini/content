**March 2019**

Held on Saturday, March 16, 19:00 UTC

Topic: **Fundraising for FOSS Projects**

[https://www.youtube.com/watch?v=TYFdjY63XTQ>](https://www.youtube.com/watch?v=TYFdjY63XTQ%3E) - Anatomy of Successful Open Source Communities and Projects (10 min)

[https://www.youtube.com/watch?v=MigSMCc8bmM>](https://www.youtube.com/watch?v=MigSMCc8bmM%3E) - How to raise money for open source projects (25 min)

<https://www.youtube.com/watch?v=TYFdjY63XTQ> - Fundraising for Projects (61 min)

Hosts: Bryce & Michèle



TAKEAWAYS FROM VIDEOS AND DISCUSSION NOTES / POINTS FROM INKSCAPERS IN ATTENDANCE 

VIDEO 1: Anatomy of Successful Open Source Communities and Projects 

NOTES FROM VIDEO:

To build healthy communities:

- Unify - share clear "why" to contribute
- Simplify - make it easy to contribute
- Incentivize - give effective motivation
- Passion - visible passion for your community



Remember to be

- Authentic
- Passionate
- Strategic
- Intentional
- Data-driven
- Open minded and flexible



TAKEAWAYS FROM INKSCAPERS

* Developer Incentives:
  -Offer official Inkscape Certification. Bronze, Silver & Gold Certificate for involvement and Code integration.
  -Offer Inkscape References. Might be useful to young Dev’s seeking employment to show evidence of their open source achievements with Inkscape.

* Regular Weekly News Updates:
  -Community engagement is born from interaction… 
  -Regular news pushed via Social media outlets, will increase interest in the project.
  -Social Media Teams need a place to feedback User concerns and requests and get responses (engagement). Communities need to know they are being heard.
  -Communities (Forum, FB, Twitter etc…) should be involved in the direction the project takes. A simple poll would create superb community interaction.

* How we can measure our community and the impact(s) we're having more accurately.

* Interesting discussions held on "why" at the hackfest. Getting clarity there would be huge in being able to properly message fundraising events

* The first thing that pops into a persons mind when you ask them to donate/support/buy, consciously or subconsciously, is WHY. And we don't have that foundation clarified. Every campaign we may think of will always need to answer that question first

* Why Inkscape exists, but also why anyone should donate, TWO Whys that need to be clarified both in terms of the cost : revenue ratio of different fundraisers, and also in terms of what fundraisers will stimulate our users and contributors to get move involved, vs. ones that risk having the opposite effect.
* Resource: Simon Sinek's Start with Why: How Great Leaders Inspire Everyone to Take Action - https://www.powells.com/book/-9781591846444 

* Stay "Open Minded" to ideas we might not have considered yet

* Look at other projects, such as Krita - Windows App and Steam store revenues



QUESTIONS TO CONSIDER: 

1.Why / what do you contribute to Inkscape?

2.What motivates you to continue contributing?

3.Name one thing that we can do to make it easier for you?



* We need to identify our User base, evaluate their needs and decide if we meet those needs, or are we moving in the wrong direction?
* Is our “RoadMap” too broad? Should we narrow the focus to consolidate effort? This would allow us to clearly define Fundraising opportunities.

* It doesn't seem we take care of the audience needs too much, there doesn't seem to be a lot of resources to go that direction.

* Patreon for developers



VIDEO 2: How to raise money for open source projects 

NOTES FROM VIDEO:

- #1 "I want the whole world to be my user" - no
  \* Won't fit everyone, esp. in tech tools
  \* Know the user. Put effort into getting to know them.
  \* Use user-centered approach. UX design.

  

- \#2 "In politics, nobody ever listens. I want to change that."
  \* Don't make assumptions without explaining how you got to them
  \* Check your point. Back up premises.

  

- #3 "There is no usable open-source email client for Android"

  - Just plain wrong, due to insufficient research
  - Pays off to take time and do research into what other people have done so far.
  - What makes your idea special compared with other solutions out there?
  - Consider joining/contributing to existing efforts.
  - Talk about the ecosystem you're in

  

- #4 "I will build a platform that will..."

  - Platforms aren't magical solutions
  - Build it, they may not come
  - Hard part is engaging with communities, and figure out communication strategies.
  - How does it fit within the context

  

- #5 "Like tinder but for..."

  - Mechanism that works in one context doesn't necessarily work in another.
  - Method Follows Problem
  - Analyze the problem first, THEN find a suitable solution
  - Don't just trust yourself, look to the community to understand what you are.

  

- #6 "A command-line tool checking SQL statement"
  \* For a tiny group of users. "Proposals for the 1%"
  \* Explain why they're important, what the larger impact is
  \* Ask better questions, to find the right balance between too narrow and too broad

  

- #7 "My tool is the pebble that will start a landslide"

  - Be realistic
  - Focus on the concept and problem
  - No need to exaggerate
  - Most successful projects know exactly what they want, and aren't afraid to convey that in realistic and honest terms, even though they lose some money, the money you do get is much more solid.



TAKEAWAYS FROM INKSCAPERS: 

* We need to identify our User base, evaluate their needs and decide if we meet those needs, or are we moving in the wrong direction?
* Is our “RoadMap” too broad? Should we narrow the focus to consolidate effort? This would allow us to clearly define Fundraising opportunities.
* Are we forking from other projects, or are we trying to re-invent the wheel?
  CMYK exists in Scribus, Krita and Gimp. Can we fork from those projects or collaborate?

* Do we publicise our achievements enough? Do we reach out to User groups making them aware of features useful to them? Putting information into a Wiki is good for archiving but it doesn’t increase interest or encourage donations from happy users. “engagement is born from interaction…”

* We need to identify our “Big Reward” bugs, those which will benefit our Users and ultimately encourage Donations from happy Users.
* As part of the “Regular News” we could publish a “Bug Poll”. A way for our communities to help us decide where effort is most needed. (no promises of a fix but does give a clear matrix of User need and where to allocate funds etc…) People love clicking on a Poll…
* Inkscape is only as strong as it’s user base. People love Inkscape but they will go to software that fulfills their needs.
* We have to stay relevant to our Users. If our users have to go and buy proprietary software, that equates to Donations lost!

* Having CMYK will enable us to compete within the pro design space, too, and enable artists and designers to work with different platforms and manufacturers - and want to give back to Inkscape!

* Publicizing achievements is a great suggestion, we ought to do more of. There is often a disconnect where users are approaching us about "big" features, yet much of what gets done day to day are more incremental or narrow focused features













