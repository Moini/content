# Inkscape Forum Rules and Guidelines, PROPOSAL

`(This is a proposal for the Django-integrated forum.  If we ever have to switch to traditional forum software, this doc will have to be edited somewhat.)

These Rules and Guidelines can be amended in the future, if necessary.


# **GUIDELINES**

## **Forum Etiquette**   
 
- Please try to solve your Inkscape problem on your own before you ask for help. We don't expect you to spend hours at it, but we can usually tell when you didn't even try. We may invite you to try, if we think you didn't. Archives of the old forum:  http://alpha.inkscape.org/vectors/www.inkscapeforum.com/, FAQ:  https://inkscape.org/learn/faq/, Help menu > Inkscape manual, Tutorials pages on the website:  https://inkscape.org/learn/tutorials/ and https://inkscape.org/learn/videos/, brynn's curated tutorials: https://forum.inkscapecommunity.com/index.php

- Please be polite.  Please be patient.  Sometimes it may take several exchanges before we understand the problem clearly, and this is especially common when someone is not writing in their native language.  This is primarily an English language forum.  Occasionally someone will be available to answer in another language, but not always.  Patience and politeness can smooth out frustrations, before it starts.

- Describe your problem or question with as much detail as possible.  Please consider providing screenshots, SVG files or videos, if necessary to explain your problem well.  For newbies, this message explains how to make a screenshot and put it in a message, and also how to attach a file. (https://inkscape.org/forums/questions/how-to-make-a-screenshot-and-put-it-in-a-message/)  Or you're also welcome to upload these files anywhere you like, and simply give us the link to them.
 
- If your question is about a problem in a specific file, or if you think you've found a bug, we most probably will need to see the SVG file.  At the very least, we would need a screenshot of the problem (such as a PNG, JPG, or GIF).

- Also please tell us your Inkscape version and operating system.

- Please don't post your problem or question in someone else's topic, unless you know *beyond a shadow of a doubt*  that your problem is the same thing. Please start your own topic.  Also please start a new topic for each different problem.

- Please only post on one forum / website. Those of us who answer all the questions don't have time to answer the same question which has already been answered in another forum / website. If by posting in more than one forum you hope to learn different techniques, just ask. We likely know them all, but might not have time to type them all out. However if you ask, we're happy to share.
 
- Please don't use personal messages for support, unless someone in the forum invites you to.

- Please don't type in all caps (unless you intend to be seen as shouting). (If you feel like shouting, please take a breath first.  We don't mind answering well-thought-out complaints; however, unrestrained venting is not appreciated, and such messages could be moderated (i.e. edited).)
 
- Please write a title which gives us a clue to what your question is about. Titles like "helppppppp!!!!!" don't give us a clue. Titles like "Circle tool not working" are better, but still a bit vague. Titles like "Circle tool draws invisible circles" are great!


## **Regular Members**  

The Inkscape Community Code of Conduct (CoC) clearly outlines the kind of behavior which forum members are expected to exhibit.  This includes both behavior towards other members, as well as what kind of images can be uploaded.

https://inkscape.org/community/coc/


## **Complaints**  

The forum also follows the CoC regarding complaints.  Generally, complaints should be made to a moderator.  Moderators will collaborate on resolving it.  If the resolution is satisfactory, the matter will be considered closed.  If the resolution is not satisfactory, the complaint should be escalated to [still to be decided].

If the complaint is about a moderator's actions, it should first be made to a different moderator.  Then all the moderators will review the matter and discuss appropriate resolutions.  If necessary, either the moderators or the complainant may escalate the problem to the Inkscape Board of Developers for a final resolution.

https://inkscape.org/community/coc/

# **RULES**

## **Forum Team**  

### Type of team members

The forum will only have moderators, since no admin positions have been built into the software.  (There are admins for the website as a whole, but not on the forum team.)  But if we ever switch to traditional forum software, we'll need to rewrite this section to include admins.

### How are team positions filled

When a moderator position is available, other moderators (as a group) will post an announcement, asking for volunteers. Volunteers will contact moderators privately and explain how they meet the prerequisites for being a moderator. The forum team (moderators) will discuss the applicants and choose the one who seems to be the best fit.

### Prerequisites for being a moderator

* be an active forum member for at least 6 months (or if no applicants have that much, then the member with the longest membership would be preferred)
* knowledge of or involvement with the development community (such as willingness to file a bug report or feature request, being a mailing list subscriber, or helping with the Inkscape website) would be a plus, for consideration, but not required
* have some past experience as a forum moderator, specifically in being able to identify spam
* be available to visit the forum at least 3 times per week, and have approx 15 minutes available on those days, to approve new messages

### Moderator Duties and Responsibilities

1. In this forum, the primary job of a moderator is to approve new messages, delete spam and ban spammers.  Moderating for behavior, as opposed to moderating for contents, is rare.  A moderator can decide whether they want to take a public role in moderating for behavior.  Or they may decide which issues they want to be pubicly involved in.  However, moderators are expected to participate in the private collaborations on behavior, whether they post messages about it or not.  (In this way, new moderators may learn from more experienced moderators, without risking any reputation.)
2. Contents moderation:
    * Most spam needs to be completely deleted and members responsible for spamming need to be banned. A good rule of thumb is if a new member's first message isn't about Inkscape or vector graphics, graphics in general, or open-source software, it's probably not appropriate for the forum.
    * Sometimes people post a message that has something to do with Inkscape (sometimes it's copied from another topic or an earlier message on the same topic), but with a spam link included. If we have time and can prove it was copied, it be can deleted and the member can be banned. Otherwise we have to delete the spam and let the Inkscape info remain. Since the member who does this won't be banned, they need to be put on the watch list in the Moderators board, so that their future posts can be monitored. (https://inkscape.org/forums/moderators/watch-list/)
1. Behavior moderation. Moderators should discuss together the best way to handle members who are misbehaving, when possible.  For convenience, this is the moderators board:  https://inkscape.org/forums/moderators/
1. If there is a behavior issue which can't wait for collaboration, here are some guidelines for comments you can post, which hopefully will buy some time for a 2nd moderator to see the message and get involved in the discussion.  Note that these are only suggestions for what a moderator *could* post in different situations. Moderators should use whatever language they can best express themselves with in a given situation. The main point is to try to gently cool down the situation and not come off as being heavy-handed.
    * For foul language only, you could post something like "Let's maintain civil discussions" or  remind the member that "This is a family-friendly forum".
    * If a member is being name-called, insulted or attacked, you could post something like "Let's not get personal" or "Please contact a moderator if you have a complaint about another member".  It's important to react to this sooner than later, because being on the receiving end, it's hard to resist defending oneself.  If the moderator does the defending *first*, it can prevent a heated discussion from happening.
    * If a discussion is *becoming* heated, post something like "Let's try to be calm" or "cool" or "civil". If it's *already*  heated, ask the participants not to post for 24 hours (cooling-off period). It's probably better not to lock a topic without consulting with another moderator. However, if the messages are blatantly offensive *and* coming minutes (rather than hours) apart, locking the thread might be your best option. Be sure and post a message that the topic is being temporarily locked to allow for "a cooling-off period". Don't forget to delete any foul language, expletives or swear words.
    * Also, be sure to post a link to the CoC, so that everyone is reminded of both the unacceptable behavior and the complaint options.
    * Then start a discussion with the rest of the moderators in the Moderator's board.
    * Even if the issue does not require much discussion, the facts should be recorded for any official complaint.
1. If a member makes a complaint to a single moderator, he or she should share that with the rest of the moderators (by posting on the moderation board), so that everyone is aware.  
		Typically moderators would talk separately to the members involved in the conflict. Since most problems are caused by misunderstandings, the moderator would explain to the member bringing forward the complaint, the perspective of the member in question and vice versa.  Hopefully everyone can shake hands and walk away. If it seems like the parties would never apologize to each other, then the moderators should tell both parties separately that the rules have been explained to the other party, and that everyone understands the expected behavior. If the parties are quite hostile towards each other, perhaps the moderator should suggest they not respond to each other's messages.  If the parties involved are satisfied with the resolution, then the matter can be considered closed. If not, then the problem will need to be escalated to "still to be decided".
1. If the complaint is about a moderator's actions, it should first be brought to other moderators, and all the moderators will discuss it together.  Moderators should be honest and try to recognize the perspective of the complainer.  They should be open to the idea that they could be wrong, willing to see the situation as a learning experience, and willing to apologize, if they realize it's appropriate.  If the forum team can't reach a concensus, or find a resolution, then the complaint needs to be escalated to the Board (as stipulated in the CoC).
1.  As a team, the moderators will also make all the decisions necessary for running the forum.  For example, this team will decide how to name and organize the different boards in the forum.  If we realize that any new new boards need to be created, at some time in the future, this team would make that happen.  Or if any new items need to be added to the Etiquette section of the Guidelines, this team would make that happen.
1.  This team might also have a lot to say about the styling or formatting of the forum, either when necessary, or when the whole website is upgraded.

### Resigning from the forum team

When a moderator realizes they are no longer able to fulfill their duties, they should consider resigning, so that a more active moderator can be selected. One month's notice is preferred, if possible.

### Inactive moderators

If other moderators realize that a moderator has become inactive or rarely active, they may ask that moderator to resign. If the inactive moderator cannot be contacted, after 6 months of attempted contact, they will be reinstated as a regular member and lose their moderation permissions. Then the process to select a new moderator will begin.





 
 